create database DBSelling318
go 
use DBSelling318
go

create table tblItem(
	tblItemID int identity(1,1),
	ItemCode varchar(13) primary key not null,
	ItemName varchar(30) not null,
	BuyingPrice decimal(18,4) not null,
	SellingPrice decimal(18,4) not null,
	ItemAmount int not null,
	Pieces varchar(15) not null
)
go

