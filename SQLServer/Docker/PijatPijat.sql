create database PijatPijat
go 

use PijatPijat
go

create table Booking(
	ID int identity(1,1),
	NoBooking varchar(5) primary key,
	Tanggal DateTime,
	MemberID varchar(5)
)

create table Layanan(
	ID int identity(1,1),
	Kode varchar(5) primary key,
	Tipe varchar(20),
	Keterangan varchar(50),
	Harga decimal(18,2)
)

create table Grade(
	ID int identity(1,1),
	Kode varchar(5) primary key,
	Grade varchar(20)
)

create table Karyawan(
	ID int identity(1,1),
	Kode varchar(5) primary key,
	Nama varchar(30),
	Role varchar(20)
)

create table Transaksi(
	ID int identity(1,1),
	Reff varchar(10) Primary Key,
	Tanggal datetime,
	KodeKaryawan varchar(5),
	KodeCustomer varchar(5)
)

create table DetailTransaksi(
	ID int identity(1,1),
	Kode varchar(5) primary key,
	Reff varchar(10),
	KodeKaryawan varchar(5),
	KodeLayanan varchar(5),
	KodeBooking varchar(5),
	Quantity int
)

create table Customer(
ID int identity(1,1) not null,
Kode varchar(5) primary key not null,
Nama varchar(50) not null,
Alamat varchar(50) not null,
Kota varchar(50) not null,
Provinsi varchar(20) not null,
MemberID varchar(5) not null,
Grade varchar(20) not null
)

insert into Customer
values
('C0001', 'Bayu', 'Jl.Garuda', 'Tangerang Selatan', 'Banten', 'M0982','G0002'),
('C0002', 'Bodi', 'Jl.Merak', 'Bogor', 'Jawa Barat', 'M0981','G0001'),
('C0003', 'Mayang', 'Jl.Titimplik', 'Depok', 'Jawa Barat', 'M0983','G0002'),
('C0004', 'Nina', 'Jl. Galaxy', 'Bekasi', 'Jawa Barat', 'M0984','G0004'),
('C0005', 'Bila', 'Jl.Tanah Abang', 'Jakarta Pusat', 'Jakarta', 'M0985','G0004'),
('C0006', 'Dimas', 'Jl.Gandaria', 'Jakarta Selatan', 'Jakarta', 'M0986','G0003')

insert into Booking values
('B0099','04-11-2023','M0984'),
('B0097','04-18-2023','M0983'),
('B0098','04-18-2023','M0982')

select * from Booking
select * from Customer

--bulan/tanggal/tahun

insert into Karyawan 
values
('K0001', 'Wahyu', 'Masseur'),
('K0002', 'Nino', 'Kasir')

select * from karyawan

insert into Grade
values
('G0001', 'Platinum'),
('G0002', 'Gold'),
('G0003', 'Silver'),
('G0004', 'Bronze')
select * from Grade

insert into Layanan
values
('L0101', 'Massage', 'FullBody', '120000'),
('L0102', 'Massage', 'HalfBody', '100000'),
('L0103', 'Massage', 'Foot Massage','60000'),
('L0104', 'Massage', 'Beck Neck Shoulder', '90000'),
('L0201', 'Goods', 'Minyak Terapi', '25000'),
('L0202', 'Goods', 'Wedang Jahe', '16000'),
('L0203', 'Goods', 'Handuk', '46500')
select * from Layanan

insert into Transaksi values
('M23010923',getdate(),'K0001','C0001'),
('M23010924',getdate(),'K0002','C0001')

select * from Transaksi

insert into DetailTransaksi values 
('D0001','M23010923','K0001','L0101','B0098',1),
('D0002','M23010923','K0001','L0201','B0098',1),
('D0003','M23010923','K0001','L0202','B0098',1)

select * from DetailTransaksi

select 
	t.Tanggal [Date], 
	t.Reff, b.NoBooking, 
	b.Tanggal, 
	c.MemberID, 
	c.Nama, 
	c.Alamat, 
	c.Kota, 
	c.Provinsi, 
	g.Grade, 
	l.Tipe,
	l.Keterangan, 
	Masseur.Nama as Masseur,
	kasir.Nama as Kasir,
	l.Harga as Price,
	dt.Quantity,
	case 
		when l.Tipe = 'Massage' then (((dt.Quantity * l.Harga) * 5) / 100)
		else isnull((l.Harga*0),0)
	end as [Service],
	((l.Harga * 11)/100) as tax,
	case 
		when l.Tipe = 'Massage' then ((dt.Quantity * l.Harga) + ((l.Harga * 5) / 100) + ((l.Harga * 11)/100))
		else isnull(((dt.quantity * l.Harga) + ((l.Harga * 11)/100)),0)
	end as Payment
from 
Transaksi t
join DetailTransaksi dt on t.Reff = dt.Reff
join (
	select k.Kode, k.Nama from
	Karyawan k
	where k.Role = 'Kasir'
) kasir on t.KodeKaryawan = kasir.Kode
join (
	select k.Kode, k.Nama from
	Karyawan k
	where k.Role = 'Masseur'
) Masseur on dt.KodeKaryawan = Masseur.Kode
join Booking b on dt.KodeBooking = b.NoBooking
join Customer c on b.MemberID = c.MemberID
join Grade g on c.Grade = g.Kode
join Layanan l on dt.KodeLayanan = l.Kode

