--02 Buatlah query untuk mengubah column Nama_Dosen dengan type data VarChar dengan panjang 200 pada table Dosen
ALTER TABLE Dosen
ALTER COLUMN Nama_Dosen VARCHAR(200)

--03 Buatlah query untuk menampilkan data berikut:
--Kode_Mahasiswa	Nama_Mahasiswa	Nama_Jurusan		Agama			
--M001				Budi Gunawan	Teknik Informatika	Islam
SELECT Kode_Mahasiswa, Nama_Mahasiswa, Nama_Jurusan, Deskripsi
FROM Mahasiswa MHS 
JOIN Jurusan JUR ON
	MHS.Kode_Jurusan = JUR.Kode_Jurusan
JOIN Agama AGM ON
	MHS.Kode_Agama = AGM.Kode_Agama
WHERE Kode_Mahasiswa = 'M001'

SELECT * FROM Mahasiswa
--04 Buatlah query untuk menampilkan data mahasiswa yang mengambil jurusan dengan Status Jurusan = Non Aktif
SELECT MHS.*, Status_Jurusan
FROM Mahasiswa MHS
JOIN Jurusan JUR ON
	MHS.Kode_Jurusan = JUR.Kode_Jurusan
WHERE JUR.Status_Jurusan = 'Non Aktif'

--05 Buatlah query untuk menampilkan data mahasiswa dengan nilai diatas 80 untuk ujian dengan Status Ujian = Aktif
SELECT MHS.*, UJ.Kode_Ujian, UJ.Status_Ujian, NI.Nilai 
FROM Mahasiswa MHS
JOIN Nilai NI 
	ON MHS.Kode_Mahasiswa = NI.Kode_Mahasiswa
JOIN Ujian UJ 
	ON UJ.Kode_Ujian = NI.Kode_Ujian
WHERE Nilai > 80 AND UJ.Status_Ujian = 'Aktif'

-- 06 Buatlah query untuk menampilkan data jurusan yang mengandung kata 'sistem'.
SELECT JUR.*
FROM Jurusan JUR
WHERE Nama_Jurusan LIKE '%sistem%';

-- 07 Buatlah query untuk menampilkan mahasiswa yang mengambil ujian lebih dari 1.
SELECT mhs.Kode_Mahasiswa, mhs.Nama_Mahasiswa
FROM Mahasiswa mhs
JOIN Nilai nilai 
	ON nilai.Kode_Mahasiswa = mhs.Kode_Mahasiswa
GROUP BY mhs.Kode_Mahasiswa, mhs.Nama_Mahasiswa
HAVING COUNT(nilai.Kode_Ujian) > 1

-- 08 Buatlah query untuk menampilkan data seperti berikut:
--Kode_Mahasiswa	Nama_Mahasiswa	Nama_Jurusan	Agama	Nama_Dosen	Status_Jurusan	Deskripsi
--M001	Budi Gunawan	Teknik Informatika	Islam	Prof. Dr. Retno Wahyuningsih	Aktif	Honorer
SELECT
	m.Kode_Mahasiswa,
	m.Nama_Mahasiswa,
	j.Nama_Jurusan,
	a.Deskripsi Agama,
	d.Nama_Dosen,
	j.Status_Jurusan,
	td.Deskripsi
FROM dbo.Mahasiswa m
	JOIN dbo.Jurusan j 
		on j.Kode_Jurusan = m.Kode_Jurusan
	JOIN dbo.Dosen d 
		on d.Kode_Jurusan = j.Kode_Jurusan
	JOIN dbo.Agama a 
		on a.Kode_Agama = m.Kode_Agama
	JOIN dbo.Type_Dosen td 
		on td.Kode_Type_Dosen = d.Kode_Type_Dosen
WHERE
	m.Kode_Mahasiswa = 'M001'

-- 09 Buatlah query untuk create view dengan menggunakan data pada no. 8, beserta query untuk mengeksekusi view tersebut.
Create View viewNo9
AS 
Select
	mhs.Kode_Mahasiswa,
	mhs.Nama_Mahasiswa,
	jur.Nama_Jurusan,
	agm.Deskripsi as 'Agama',
	dos.Nama_Dosen,
	jur.Status_Jurusan,
	td.Deskripsi 'Tipe Dosen'
From Mahasiswa mhs
join Jurusan jur on mhs.Kode_Jurusan = jur.Kode_Jurusan
join Agama agm on mhs.Kode_Agama = agm.Kode_Agama
join Dosen dos on jur.Kode_Jurusan = dos.Kode_Jurusan
join Type_Dosen td on dos.Kode_Type_Dosen = td.Kode_Type_Dosen
-- where mhs.Kode_Mahasiswa = 'M001'
SELECT * FROM viewNo9
WHERE Status_Jurusan = 'Aktif'

-- 10 Buatlah query untuk menampilkan data mahasiswa beserta nilainya (mahasiswa yang tidak punya nilai juga ditampilkan).
select mhs.*, Nilai
from Mahasiswa As mhs
left join nilai as nil 
	on mhs.Kode_Mahasiswa = nil.Kode_Mahasiswa

--11	Buatlah query untuk menampilkan data mahasiswa beserta nilainya yang memiliki nilai:
--a. Minimum
select Nama_Mahasiswa, Nilai [Minimum]
from Nilai nilai
join Mahasiswa mhs on nilai.Kode_Mahasiswa = mhs.Kode_Mahasiswa
where Nilai = (select MIN(Nilai) from nilai)
--b. Maximum
select Nama_Mahasiswa, Nilai [Maksimum]
from Nilai nilai
join Mahasiswa mhs on nilai.Kode_Mahasiswa = mhs.Kode_Mahasiswa
where Nilai = (select MAX(Nilai) from nilai)
--c. Diatas rata-rata
SELECT mhs.*,Nilai
FROM Nilai nilai
JOIN Mahasiswa mhs on  nilai.Kode_Mahasiswa = mhs.Kode_Mahasiswa
WHERE nilai.Nilai > (SELECT AVG(Nilai) FROM Nilai)
--d. Dibawah rata-rata
SELECT mhs.*,Nilai
FROM Nilai nilai
JOIN Mahasiswa mhs on  nilai.Kode_Mahasiswa = mhs.Kode_Mahasiswa
WHERE nilai.Nilai < (SELECT AVG(Nilai) FROM Nilai)
-- 12 	Tambahkan kolom Biaya dengan type data Money/Number/Decimal 18,4 pada table Jurusan
ALTER TABLE Jurusan 
ADD biaya decimal(18,4)
-- 13 Tambahkan nilai kolom Biaya pada table Jurusan sebagai berikut:
--Nama_Jurusan	Biaya					
--Teknik Informatika	                  1,500,000					
--Management Informatika	                  1,550,000					
--Sistem Informasi	                  1,475,000					
--Sistem Komputer	                  1,350,000					
--Komputer Akuntansi	                  1,535,000		
update Jurusan set Biaya=1500000 where Kode_Jurusan='J001'
update Jurusan set Biaya=1550000 where Kode_Jurusan='J002'
update Jurusan set Biaya=1475000 where Kode_Jurusan='J003'
update Jurusan set Biaya=1350000 where Kode_Jurusan='J004'
update Jurusan set Biaya=1535000 where Kode_Jurusan='J005'
-- 14 	Buatlah query untuk menampilkan jurusan dengan biaya:
-- Soal No 14 a
SELECT *
FROM Jurusan
WHERE Biaya = ( SELECT MIN(Biaya) FROM Jurusan )

-- Soal No 14 b
SELECT *
FROM Jurusan
WHERE Biaya = ( SELECT MAX(Biaya) FROM Jurusan )

-- Soal No 14 c
SELECT *
FROM Jurusan
WHERE Biaya > ( SELECT AVG(Biaya) FROM Jurusan)

--Soal No 14 d
SELECT *
FROM Jurusan
WHERE Biaya < ( SELECT AVG(Biaya) FROM Jurusan )
