create database DBMahasiswa318
go
use DBMahasiswa318
go

create table Jurusan (
Id int identity(1,1),
Kode_Jurusan char(5) primary key not null,
Nama_Jurusan varchar(50) not null,
Status_jurusan varchar(100) not null,
)

insert into Jurusan values
('J001', 'Teknik Informatika','Aktif'),
('J002', 'Management Informatika','Aktif'),
('J003', 'Sistem Informasi','Non Aktif'),
('J004', 'Sistem Komputer','Aktif'),
('J005', 'Komputer Akuntansi','Non Aktif')

select * from Jurusan

create table Agama (
Id int identity(1,1),
Kode_Agama char(5) primary key not null,
Deskripsi varchar(20) not null,
)

insert into Agama values 
('A001','Islam'),
('A002','Kristen'),
('A003','Katolik'),
('A004','Hindu'),
('A005','Budha')

select * from Agama

create table Mahasiswa (
Id int identity(1,1),
Kode_Mahasiswa char(5) primary key not null,
Nama_Mahasiswa varchar(100) not null,
Alamat varchar(200) not null,
Kode_Agama char(5) not null,
Kode_Jurusan char(5) not null,
)

insert into Mahasiswa values
('M001','Budi Gunawan', 'Jl. Mawar No 3 RT 05 Cicalengka, Bandung', 'A001','J001'),
('M002','Rinto Raharjo', 'Jl. Kebagusan No. 33 RT04 RW06 Bandung', 'A002', 'J002'),
('M003','Asep Saepudin', 'Jl. Sumatera No. 12 RT02 RW01, Ciamis', 'A001', 'J003'),
('M004', 'M. Hafif Isbullah', 'Jl. Jawa No 01 RT01 RW01, Jakarta Pusat', 'A001','J001'),
('M005','Cahyono','Jl. Niagara No. 54 RT01 RW09, Surabaya', 'A003','J002')

select * from Mahasiswa

create table Ujian(
Id int identity(1,1),
Kode_Ujian char(5) primary key not null,
Nama_Ujian varchar(50) not null,
Status_Ujian varchar(100) not null,
)

insert into Ujian values
('U001', 'Algoritma', 'Aktif'),
('U002','Aljabar', 'Aktif'),
('U003','Statistika', 'Non Aktif'),
('U004','Etika Profesi', 'Non Aktif'),
('U005', 'Bahasa Inggris', 'Aktif')

select * from Ujian

create table Type_Dosen(
Id int identity(1,1),
Kode_TypeDosen char(5) primary key not null,
Deskripsi varchar(20) not null
)

insert into Type_Dosen values
('T001', 'Tetap'),
('T002', 'Honorer'),
('T003','Expertise')
go
select * from Type_Dosen

create table Dosen(
Id int identity(1,1),
Kode_Dosen char(5) primary key not null,
Nama_Dosen varchar(100) not null,
Kode_Jurusan char(5) not null,
Kode_Type_Dosen char(5) not null,
)

insert into Dosen values
('D001', 'Prof. Dr. Retno Wahyuningsih', 'J001','T002'),
('D002', 'Prof. Roy M. Sutikno', 'J002', 'T001'),
('D003', 'Prof. Hendri A. Verburgh','J003','T002'),
('D004','Prof. Risma Suparwata','J004','T002'),
('D005','Prof. Amir Sjarifuddin Madjid, MM, MA','J005','T001')
go
select * from Dosen

create table Nilai(
Id int identity(1,1) primary key,
Kode_Mahasiswa char(5) not null,
Kode_Ujian char(5) not null,
Nilai Decimal(8,0) not null
)

insert into Nilai values
('M004','U001',90),
('M001','U001',80),
('M002','U003',85),
('M004','U002',95),
('M005','U005', 70)
go
select * from Nilai

--soal nomor2 ubah column nama_dosen dengan type data varchar 200
alter table Dosen alter column Nama_Dosen varchar(200)

-- soal nomor 3 query menampilkan kode_mahasiswa, nama_mahasiswa, nama_jurusan, agama
select m.Kode_Mahasiswa, m.Nama_Mahasiswa, j.Nama_Jurusan, a.Deskripsi
from Mahasiswa as m join Jurusan as j 
on m.Kode_Jurusan = j.Kode_Jurusan
join Agama as a on m.Kode_Agama = a.Kode_Agama
where m.Kode_Mahasiswa = 'M001'

--soal nomor 4 menampilkan mahasiswa dgn status jurusan non aktif
select m.Kode_Mahasiswa, m.Nama_Mahasiswa, j.Nama_Jurusan, j.Status_jurusan
from mahasiswa as m 
join jurusan as j on m.Kode_Jurusan = j.Kode_Jurusan
where j.Status_jurusan = 'Non Aktif'

--cross join akan menampilkan semua data
--select m.Kode_Mahasiswa, m.Nama_Mahasiswa, j.Nama_Jurusan, j.Status_jurusan
--from mahasiswa as m 
--cross join jurusan as j
--where j.Status_jurusan = 'Non Aktif'

--soal nomor 5 menampilkan mahasiswa dgn status ujian aktif
select m.*, n.Nilai, u.Status_Ujian from
mahasiswa as m
join Nilai as n on m.Kode_Mahasiswa = n.Kode_Mahasiswa
join Ujian as u on u.Kode_Ujian = n.Kode_Ujian
where n.Nilai > 80 and u.Status_Ujian = 'Aktif'

--soal no 6 tampilkan data jurusan yg mengandung kata sistem
select * from Jurusan where Nama_Jurusan
LIKE '%sistem%'

--soal no7 tampilkan mahasiswa yang mengambil ujian lebih dari 1
select m.Kode_Mahasiswa, m.Nama_Mahasiswa, count(n.Kode_Mahasiswa) as record_ujian from
mahasiswa as m
inner join Nilai as n on n.Kode_Mahasiswa = m.Kode_Mahasiswa
inner join Ujian as u on u.Kode_Ujian = n.Kode_Ujian
group by m.Nama_Mahasiswa, m.Kode_Mahasiswa
having count(n.Kode_Mahasiswa) > 1

--soal no 8 menampilkan Kode_Mahasiswa	Nama_Mahasiswa	Nama_Jurusan	Agama	Nama_Dosen	Status_Jurusan	Deskripsi
select m.Kode_Mahasiswa, m.Nama_Mahasiswa, j.Nama_Jurusan, a.Deskripsi as Agama, d.Nama_Dosen, j.Status_jurusan, t.Deskripsi as Deskripsi
from mahasiswa as m
join agama as a on m.Kode_Agama = a.Kode_Agama 
join jurusan as j on m.Kode_Jurusan = j.Kode_Jurusan
join Dosen as d on j.Kode_Jurusan = d.Kode_Jurusan
join Type_Dosen as t on d.Kode_Type_Dosen = t.Kode_TypeDosen
where
	m.Kode_Mahasiswa = 'M001'

-- soal no 9 membuat create view dari no 8
create view VWMahasiswa as select m.Kode_Mahasiswa, m.Nama_Mahasiswa, j.Nama_Jurusan, a.Deskripsi as Agama, d.Nama_Dosen, j.Status_jurusan, t.Deskripsi as Deskripsi
from mahasiswa as m
join agama as a on m.Kode_Agama = a.Kode_Agama 
join jurusan as j on m.Kode_Jurusan = j.Kode_Jurusan
join Dosen as d on j.Kode_Jurusan = d.Kode_Jurusan
join Type_Dosen as t on d.Kode_Type_Dosen = t.Kode_TypeDosen
where
	m.Kode_Mahasiswa = 'M001'

-- soal no 10 tampilkan mahasiswa dan nilainya dan tetap tampilkan yg tidak punya nilai
select m.*, n.Nilai 
from Mahasiswa as m
left join Nilai as n 
on n.Kode_Mahasiswa = m.Kode_Mahasiswa

-- soal no 11 query untuk menampilkan data mahasiswa beserta nilainya a.min, b.max, c.diatas rata2, d.dibawah rata2
-- 11 a min biasa
 select Nama_Mahasiswa, Nilai[Minimum]
	from Nilai nilai
	join Mahasiswa mhs on nilai.Kode_Mahasiswa = mhs.Kode_Mahasiswa
	where Nilai = (select MIN(Nilai) from nilai)
--11 a min pakai top 1
SELECT TOP 1 mhs.*,Nilai
FROM Nilai nilai
JOIN Mahasiswa mhs on  nilai.Kode_Mahasiswa = mhs.Kode_Mahasiswa
ORDER BY Nilai

-- 11 b max
 select Nama_Mahasiswa, Nilai as Maximum
	from Nilai nilai
	join Mahasiswa mhs on nilai.Kode_Mahasiswa = mhs.Kode_Mahasiswa
	where Nilai = (select Max(Nilai) from nilai)

select AVG(Nilai) as rata_rata from Nilai

-- 11 c yg salah
select n.Nilai as Diatas_Rata_Rata
from Nilai as n
group by n.Nilai
having n.Nilai > 84
--11 c with subquery
SELECT mhs.Kode_Mahasiswa, mhs.Nama_Mahasiswa, Nilai
FROM Nilai nilai
JOIN Mahasiswa mhs on  nilai.Kode_Mahasiswa = mhs.Kode_Mahasiswa
WHERE nilai.Nilai > 
(
SELECT AVG(Nilai)
FROM Nilai
)

select n.Nilai as Dibawah_Rata_Rata
from Nilai as n
group by n.Nilai
having n.Nilai < 84

-- soal no 12
alter table Jurusan add Biaya decimal(18,4)

-- soal no13
update Jurusan set Biaya =  1500000 where Nama_Jurusan = 'Teknik Informatika'
update Jurusan set Biaya =  1550000 where Nama_Jurusan = 'Management Informatika'
update Jurusan set Biaya =  1475000 where Nama_Jurusan = 'Sistem Informasi'
update Jurusan set Biaya =  1350000 where Nama_Jurusan = 'Sistem Komputer'
update Jurusan set Biaya =  1535000 where Nama_Jurusan = 'Komputer Akuntansi'

--soal no 14
select min(biaya) as minimum from Jurusan
select max(biaya) as maximum from Jurusan

select AVG(Biaya) as rata_rata from jurusan

select j.Biaya as Diatas_Rata_Rata
from Jurusan as J
group by j.Biaya
having j.Biaya > 1482000

select j.Biaya as Dibawah_Rata_Rata
from Jurusan as J
group by j.Biaya
having j.Biaya < 1482000

select * from agama
select * from mahasiswa
select * from nilai
select* from ujian
select * from Dosen
select * from jurusan