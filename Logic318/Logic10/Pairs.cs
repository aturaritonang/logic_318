﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic10
{
    internal class Pairs
    {
        public Pairs()
        {
            Console.WriteLine("=== Pairs ===");
            Console.WriteLine("Nilai : ");
            int target = int.Parse(Console.ReadLine());
            Console.Write("Data: ");
            int[] data = Array.ConvertAll(Console.ReadLine().Split(" "), int.Parse);
            int count = 0;
            for (int i = 0; i < data.Length; i++)
            {
                for (int j = 0; j < data.Length; j++)
                {
                    if (i == j)
                        continue;
                    if (data[j] - data[i] == target)
                        count++;
                }
            }
            Console.WriteLine($"Total Target: {count}");
        }

    }
}
