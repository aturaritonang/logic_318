﻿namespace Gateway;
public class Program
{
    //trigger untuk saat set startup project
    static void Main(string[] args)
    {
        menu();
        Console.ReadKey();
    }

    static void menu()
    {
        bool ulangi = true;
        while (ulangi)
        {
            Console.WriteLine("== Welcome to batch 318 ");
            Console.WriteLine("|   1. Hari 1           |");
            Console.WriteLine("|   2. Hari 2           |");
            Console.WriteLine("|   3. Hari 3           |");
            Console.WriteLine("|   4. Hari 4           |");
            Console.WriteLine("|   5. Hari 5           |");
            Console.WriteLine("|   6. Hari 6           |");
            Console.WriteLine("|   7. Hari 7           |");
            Console.WriteLine("|   8. Hari 8           |");
            Console.WriteLine("|   9. Hari 9           |");
            Console.WriteLine("|  10. PR Hari 9        |");
            Console.Write("Pilih hari : ");
            int hari = int.Parse(Console.ReadLine());
            Console.Clear();

            switch (hari)
            {
                case 5:
                    Logic05.Program program05 = new Logic05.Program();
                    kembali();
                    break;

                case 6:
                    Logic06.Program program06 = new Logic06.Program();
                    kembali();
                    break;
                case 7:
                    Logic07.Program program07 = new Logic07.Program();
                    kembali();
                    break;
                case 8:
                    Logic08.Program program08 = new Logic08.Program();
                    kembali();
                    break;
                case 9:
                    Logic09.Program program09 = new Logic09.Program();
                    kembali();
                    break;
                case 10:
                    Logic10.Program program10 = new Logic10.Program();
                    kembali();
                    break;
                default:
                    Console.WriteLine("Tidak ada dalam menu");
                    kembali();
                    break;
            }
            ulangi = false;
        }
    }
    static void kembali()
    {
        Console.WriteLine("\nApakah ingin kembali ke menu utama ? (y/n) ");
        string menuInput = Console.ReadLine().ToLower();
        if (menuInput == "y")
        {
            Console.Clear();
            menu();
        }
        else
        {
            Console.WriteLine("Terima kasih!");
            Console.WriteLine("Silahkan tekan Enter kembali...");
        }
    }
}