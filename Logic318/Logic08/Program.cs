﻿namespace Logic08
{
    public class Program
    {
        public Program()
        {
            menu();
        }
        static void Main(string[] args)
        {
            menu();
        }
        static void menu()
        {
            Console.WriteLine("==== Welcome to Day 08 ====");
            Console.WriteLine("|   1. Insertion Sort    |");
            Console.WriteLine("|   7. Find Median       |");
            Console.WriteLine("|   0. Exit		|");
            Console.Write("Masukkan no soal: ");
            int soal = int.Parse(Console.ReadLine());

            switch (soal)
            {
                case 1:
                    tesSort soal1 = new tesSort();
                    kembali();
                    break;
                case 7:
                    FindMedian soal7 = new FindMedian();
                    kembali();
                    break;
                default:
                    break;
            }
            Console.Write("\nPress any key...");
            Console.ReadKey();
        }

        public static int Tambah(int a, int b)
        {
            return a + b;
        }

        static void kembali()
        {
            Console.WriteLine("\nApakah ingin kembali ke menu ? (y/n) ");
            string menuInput = Console.ReadLine().ToLower();
            if (menuInput == "y")
            {
                Console.Clear();
                menu();
            }
            else
            {
                Console.WriteLine("Terima kasih!");
                Console.WriteLine("Silahkan tekan Enter kembali...");
            }
        }

    }

}

