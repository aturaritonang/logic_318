﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic09
{
    internal class SherlockandArray
    {
        public SherlockandArray()
        {
            Console.WriteLine("--Sherlock And Array--");
            Console.Write("input : ");
            string[] arr = Console.ReadLine().Split(' ');
            int[] arrInt = Array.ConvertAll(arr, int.Parse);

            int jmlhKiri = 0;
            int jmlhKanan = arrInt.Sum();
            string hasil = "NO";

            for (int i = 0; i < arrInt.Length; i++)
            {
                jmlhKanan -= arrInt[i];
                if (jmlhKiri == jmlhKanan)
                {
                    hasil = "YES";
                    break;
                }
                jmlhKiri += arrInt[i];
            }

            Console.WriteLine(hasil);
            //Console.WriteLine($"kiri {jmlhKiri} , kanan {jmlhKanan}");
        }
    }
}
