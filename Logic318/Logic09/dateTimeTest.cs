﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic09
{
    internal class dateTimeTest
    {
        public dateTimeTest()
        {
            Console.Write("\nTanggal pertama meminjam buku (dd/mm/yyyy) : ");
            string tanggalPertama = Console.ReadLine();
            Console.Write("\nTanggal mengembalikan buku (dd/mm/yyyy) : ");
            string tanggalMengembalikan = Console.ReadLine();
            DateTime tanggalPertamaMinjam = DateTime.Parse(tanggalPertama);
            DateTime mengembalikanTanggal = DateTime.Parse(tanggalMengembalikan);

            DateTime date2 = DateTime.Now;//jika ingin tanggal pengembalian nya hari ini tinggal kita panggil date2 dirumus

            TimeSpan span = mengembalikanTanggal - tanggalPertamaMinjam;

            int denda = span.Days * 500;

            if (span.Days <= 0)
            {
                Console.WriteLine("Masukkan tanggal awal pinjam dengan benar!");
            }
            else if (span.Days <= 3)
            {
                Console.WriteLine("Terima kasih telah mengembalikkan buku sesuai dengan maksimal pinjam 3 hari!");
                Console.WriteLine($"Total hari : {span.Days}");
            }
            else
            {
                Console.WriteLine($"Total hari : {span.Days}");
                Console.WriteLine($"Anda mengembalikkan buku lebih dari 3 hari dengan total pinjam {span.Days} hari x denda Rp.500/hari = {denda}");
            }

        }
    }
}
