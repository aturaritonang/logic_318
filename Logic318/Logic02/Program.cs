﻿using Logic02;

//switchStatement();
//inputArray();
//array2Dimensi();
//listAdd();
//listRemove();
//convertArrayAll();
//padLeft();
contain();
//listClass();
//inisialisasiList();

Console.ReadKey();

static void contain()
{
    Console.WriteLine("--Contain--");
    Console.Write("Masukkan kalimat : ");
    string kalimat = Console.ReadLine();
    Console.Write("Masukkan contain(teks/string) : ");
    string contain = Console.ReadLine();

    if (kalimat.Contains(contain))
    {
        Console.WriteLine($"Kalimat ini |{kalimat}| mengandung Contains : " + contain);
    }
    else
    {
        Console.WriteLine($"Kalimat ini |{kalimat}| tidak mengandung Contains : " + contain);
    }

}

static void padLeft()
{
    Console.WriteLine("--Pad Left--");
    Console.Write("Masukkan input : ");
    int input = int.Parse(Console.ReadLine());
    Console.Write("Masukkan panjang karakter : ");
    int panjang = int.Parse(Console.ReadLine());
    Console.Write("Masukkan Char : ");
    char chars = char.Parse(Console.ReadLine());

    Console.WriteLine($"Hasil PadLeft : {input.ToString().PadLeft(panjang, chars)}");
}

static void convertArrayAll()
{
    Console.WriteLine("--Convert Array All--");
    Console.Write("Masukkan angka array (pakai koma) : ");
    int[] array = Array.ConvertAll(Console.ReadLine().Split(","), int.Parse);

    Console.WriteLine(String.Join("\t", array));
}

static void listRemove()
{
    Console.WriteLine("--List Add--");
    List<User> listUser = new List<User>()
    {
        new User(){Name = "Abdullah Muafa", Age = 18},
        new User(){Name  = "Laudry M", Age = 21},
        new User(){Name  = "Marchelino M Sihotang", Age = 21},
    };

    listUser.RemoveAt(1);

    for (int i = 0; i < listUser.Count; i++)
    {
        Console.WriteLine(listUser[i].Name + " Sudah berumur " + listUser[i].Age + " tahun");
    }
}
static void listAdd()
{
    Console.WriteLine("--List Add--");
    List<User> listUser = new List<User>()
    {
        new User(){Name = "Abdullah Muafa", Age = 18},
        new User(){Name  = "Laudry M", Age = 21},
    };

    User users = new User();
    users.Name = "Alfi Azizi";
    users.Age = 23;

    listUser.Add(new User() { Name = "Isni Dwitiniardi", Age = 17 });
    listUser.Add(users);

    for (int i = 0; i < listUser.Count; i++)
    {
        Console.WriteLine(listUser[i].Name + " Sudah berumur " + listUser[i].Age + " tahun");
    }
}

static void listClass()
{
    List<User> listUser = new List<User>()
    {
        new User(){Name = "Abdullah Muafa", Age = 18},
        new User(){Name  = "Laudry M", Age = 21},
    };

    for (int i = 0; i < listUser.Count; i++)
    {
        Console.WriteLine(listUser[i].Name + " Sudah berumur " + listUser[i].Age + " tahun");
    }
}

static void inisialisasiList()
{
    List<string> list = new List<string>()
    {
        "John Doe",
        "Jane Doe",
        "Joe Doe"
    };

    list.Add("James Doe");

    Console.WriteLine(String.Join(", ", list));

}

static void array2Dimensi()
{
    int[,] array = new int[,]
    {
        {1, 2, 3},
        {4, 5, 6},
        {7, 8, 9},
    };

    for (int i = 0; i < 3; i++)
    {
        for (int j = 0; j < 3; j++)
        {
            Console.Write(array[i, j] + " \t");
        }
        Console.WriteLine();
    }

    //Console.WriteLine(array[0,2]); //3
    //Console.WriteLine(array[1,1]); //5
    //Console.WriteLine(array[2,1]); //7
    //Console.WriteLine(array[2,2]); //9
}

static void inputArray()
{
    int[] a = new int[5];

    for (int i = 0; i < a.Length; i++)
    {
        Console.Write($"Masukkan array ke - {i + 1} = ");
        a[i] = int.Parse(Console.ReadLine());
    }
    Console.WriteLine("Output : ");
    foreach (int i in a)
    {
        Console.WriteLine(i);
    }
}

static void arrayFor()
{
    string[] arrayStr = new string[] { "Asti", "Isni", "Marchel" };
    for (int i = 0; i < arrayStr.Length; i++)
    {
        Console.WriteLine(arrayStr[i]);
    }
}

static void arrayForeachString()
{
    string[] strArray = { "Muafa", "Laudry", "Alfi", "Marchel" };
    int temp = 0;

    foreach (string str in strArray)
    {
        Console.Write(str);
    }
}

static void arrayForeach()
{
    int[] arrayA = { 1, 2, 3 };
    int sum = 0;
    foreach (int item in arrayA)
    {
        Console.WriteLine(item);
        sum += item;
    }
    Console.WriteLine(sum);
}

static void forBersarang()
{
    for (int i = 0; i < 3; i++)
    {
        for (int j = 0; j < 3; j++)
        {
            Console.Write($"{i} , {j}");
        }
        Console.WriteLine();
    }
}

static void perulanganDoWhile()
{
    int a = 0;
    do
    {
        Console.WriteLine(a);
        a++;
    } while (a < 5);
}

static void forLoopIncrement()
{
    Console.WriteLine("For Increment");
    for (int i = 0; i < 5; i++)
    {
        Console.WriteLine($"{i}");
    }
}

static void forLoopDecrement()
{
    Console.WriteLine("For Decrement");
    for (int i = 5; i > 0; i--)
    {
        Console.WriteLine($"{i}");
    }
}

static void perulanganWhile()
{
    bool ulangi = true;
    int nilai = 1;
    while (ulangi)
    {
        Console.WriteLine($"Proses ke : {nilai}");
        nilai++;

        Console.Write($"Apakah anda akan mengulangi proses? (y/n) ");
        string input = Console.ReadLine().ToLower();

        if (input == "y")
        {
            ulangi = true;
        }
        else if (input == "n")
        {
            ulangi = false;
        }
        else
        {
            nilai = 1;
            Console.WriteLine("Input yang anda masukkan salah!");
            ulangi = true;
        }
    }
}

static void switchStatement()
{
    Console.WriteLine("Pilih buah kesukaan anda (apel / jeruk / pisang) : ");
    string pilihan = Console.ReadLine().ToLower();

    switch (pilihan)
    {
        case "apel":
            Console.WriteLine("Anda memilih buah apel");
            break;
        case "jeruk":
            Console.WriteLine("Anda memilih buah jeruk");
            break;
        case "pisang":
            Console.WriteLine("Anda memilih buah pisang");
            break;
        default:
            Console.WriteLine("Pilihan anda tidak tersedia!");
            break;
    }
}