﻿//golUpah();
fibonacci();
//soalNo4();
Console.ReadKey();

static void golUpah()
{
    float lembur = 0, total = 0, upah = 0;
    float upahLembur = 0;

    Console.WriteLine("\n--Tugas No 1 Gol Karyawan--");
    Console.WriteLine("Pilih golongan karyawan");
    Console.WriteLine("1.Golongan 1");
    Console.WriteLine("2.Golongan 2");
    Console.WriteLine("3.Golongan 3");
    Console.WriteLine("4.Golongan 4");

    Console.Write("Masukkan golongan karyawan = ");
    string golongan = Console.ReadLine().Replace(" ", "");
    Console.Write("Masukkan jumlah jam kerja karyawan = ");
    string jam = Console.ReadLine().Replace(" ", "");

    if (golongan == "" || jam == "")
    {
        if (jam == "")
        {
            Console.WriteLine("HARAP MASUKKAN JUMLAH JAM KERJA JANGAN KOSONG!");
        }
        else if (golongan == "")
        {
            Console.WriteLine("HARAP MASUKKAN GOLONGAN KERJA JANGAN KOSONG!");
        }
        else
        {
            Console.WriteLine("HARAP MASUKKAN GOLONGAN DAN JUMLAH JAM KERJA JANGAN KOSONG!");
        }
    }
    else
    {
        int gol = int.Parse(golongan);
        int jamKerja = int.Parse(jam);
        switch (gol)
        {
            case 1:
                if (jamKerja > 40)
                {
                    upah = jamKerja <= 40 ? jamKerja * 2000 : 40 * 2000;
                    lembur = jamKerja - 40;
                    upahLembur = 2000 * 1.5f * lembur;
                    total = upah + upahLembur;
                }
                else if (jamKerja <= 40 && jamKerja > 0)
                {
                    upah = jamKerja <= 40 ? jamKerja * 2000 : 40 * 2000;
                    lembur = jamKerja - 40;
                    upahLembur = 2000 * 1.5f * lembur;
                    total = upah + upahLembur;
                }
                else
                {
                    Console.WriteLine("Jam kerja tidak boleh kurang dari sama dengan 0!");
                }
                break;
            case 2:
                if (jamKerja > 40)
                {
                    upah = jamKerja <= 40 ? jamKerja * 3000 : 40 * 3000;
                    lembur = jamKerja - 40;
                    upahLembur = 3000 * 1.5f * lembur;
                    total = upah + upahLembur;
                }
                else if (jamKerja > 0 && jamKerja <= 40)
                {
                    upah = jamKerja <= 40 ? jamKerja * 3000 : 40 * 3000;
                    lembur = jamKerja - 40;
                    upahLembur = 3000 * 1.5f * lembur;
                    total = upah + upahLembur;
                }
                else
                {
                    Console.WriteLine("Jumlah jam kerja tidak boleh kurang dari 0!");
                }
                break;
            case 3:
                if (jamKerja > 40)
                {
                    upah = jamKerja <= 40 ? jamKerja * 4000 : 40 * 4000;
                    lembur = jamKerja - 40;
                    upahLembur = 4000 * 1.5f * lembur;
                    total = upah + upahLembur;
                }
                else if (jamKerja > 0 && jamKerja <= 40)
                {
                    upah = jamKerja <= 40 ? jamKerja * 4000 : 40 * 4000;
                    lembur = jamKerja - 40;
                    upahLembur = 4000 * 1.5f * lembur;
                    total = upah + upahLembur;
                }
                else
                {
                    Console.WriteLine("Jumlah jam kerja tidak boleh 0!");
                }
                break;
            case 4:
                if (jamKerja > 40)
                {
                    upah = jamKerja <= 40 ? jamKerja * 4000 : 40 * 4000;
                    lembur = jamKerja - 40;
                    upahLembur = 4000 * 1.5f * lembur;
                    total = upah + upahLembur;
                }
                else if (jamKerja > 0 && jamKerja <= 40)
                {
                    upah = jamKerja <= 40 ? jamKerja * 4000 : 40 * 4000;
                    lembur = jamKerja - 40;
                    upahLembur = 4000 * 1.5f * lembur;
                    total = upah + upahLembur;
                }
                else
                {
                    Console.WriteLine("Jumlah jam kerja tidak boleh 0!");
                }
                break;
            default:
                Console.WriteLine("Masukkan dengan benar!");
                break;
        }
        Console.WriteLine($"Upah : {upah,9}");
        Console.WriteLine($"Lembur : {lembur,9}");
        Console.WriteLine($"Bayaran Lembur : {upahLembur,9}");
        Console.WriteLine($"Total : {total,9}");
    }
}

static void soalNo2()
{
    Console.WriteLine("--Total Kata--");
    Console.Write("Masukkan kalimat : ");
    string kata = Console.ReadLine();
    string[] kataSplit = kata.Split(" ");

    for (int i = 0; i < kataSplit.Length; i++)
    {
        Console.WriteLine($"Kata {i + 1} : {kataSplit[i]}");
    }
    Console.WriteLine("Total kata adalah " + kataSplit.Length);
}

static void soalNo3()
{
    Console.Write("Masukkan kalimat : ");
    string[] kal = Console.ReadLine().Split();
    string tmp = "";

    for (int i = 0; i < kal.Length; i++)
    {
        for (int j = 0; j < kal[i].Length; j++)
        {
            if (j == 0)
            {
                tmp += kal[i][j];
            }
            else if (j == kal[i].Length - 1)
            {
                tmp += kal[i][j];
            }
            else
            {
                tmp += kal[i].Replace(kal[i], "*");
            }
            tmp += " ";
        }
    }
    Console.WriteLine(tmp);
}

static void soalNo4()
{
    Console.Write("Masukkan kalimat : ");
    string[] kal = Console.ReadLine().Split();
    string tmp = "";

    for (int i = 0; i < kal.Length; i++)
    {
        for (int j = 0; j < kal[i].Length; j++)
        {
            if (j == 0)
            {
                tmp += kal[i].Replace(kal[i], "*");
            }
            else if (j == kal[i].Length - 1)
            {
                tmp += kal[i].Replace(kal[i], "*");
            }
            else
            {
                tmp += kal[i][j];
            }
            //tmp += " ";
        }
    }
    Console.WriteLine(tmp);
}

static void fibonacci()
{
    Console.WriteLine("--Fibonacci--");
    Console.Write("Masukkan jumlah bilangan : ");
    int fibon = int.Parse(Console.ReadLine());

    int angka1 = 0;
    int angka2 = 1;
    int angka3 = 1;

    for (int i = 0; i < fibon; i++)
    {
        angka3 = angka2 + angka1;
        angka1 = angka2;
        angka2 = angka3;

        Console.Write(angka1 + " , ");
    }
    Console.WriteLine();
}

static void soalNo8()
{
    Console.WriteLine("--Persegi soal no. 8--");
    Console.Write("Masukkan angka untuk membentuk persegi : ");
    int input = int.Parse(Console.ReadLine());
    int kebalik = input;

    for (int i = 1; i <= input; i++)
    {
        for (int j = 1; j <= input; j++)
        {
            if (i == 1)
            {
                Console.Write(j + "\t");
            }
            else if (i == input)
            {
                Console.Write(kebalik-- + "\t");
            }
            else if (j == 1 || j == input)
            {
                Console.Write("*\t");
            }
            else
            {
                Console.Write("\t");
            }
        }
        Console.WriteLine("");
    }
}