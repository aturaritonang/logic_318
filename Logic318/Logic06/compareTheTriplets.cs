﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic06
{
    internal class compareTheTriplets
    {
        public compareTheTriplets()
        {
            Console.Write("Masukkan point Alice : ");
            int[] arrAlice = Array.ConvertAll(Console.ReadLine().Split(" "), int.Parse );
            Console.Write("Masukkan point Bob : ");
            int[] arrBob = Array.ConvertAll(Console.ReadLine().Split(" "), int.Parse);

            int alice = 0, bob = 0;

            for (int i = 0; i < arrAlice.Length; i++)
            {
                if (arrAlice[i] > arrBob[i])
                {
                    alice++;
                }
                else if (arrBob[i] > arrAlice [i])
                {
                    bob++;
                }
            }
            Console.WriteLine($"Point Alice : {alice}, Point Bob : {bob}");
        }
    }
}
