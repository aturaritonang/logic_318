﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic06
{
    internal class SolveMeFirst
    {
        public SolveMeFirst()
        {
            Console.Write("Masukkan nilai a: ");
            int a = int.Parse(Console.ReadLine());
            Console.Write("Masukkan nilai b: ");
            int b = int.Parse(Console.ReadLine());

            //int result = Addition(a, b);

            Console.WriteLine($"{Addition(a, b)}");
        }

        //contoh overload
        public int Addition(int value1, int value2)
        {
            return value1 + value2;
        }
        public int Addition(string value1, int value2)
        {
            return value2;
        }
        public int Addition(int value1, int value2, int value3)
        {
            return value1 + value2 + value3;
        }

    }
}
