﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utility;

namespace Logic06
{
    internal class DiagonalDifference
    {
        public DiagonalDifference()
        {
            Console.Write("Masukkan nilai n:");
            int size = int.Parse(Console.ReadLine());
            int[,] matrix = new int[size, size];
            int mainDiagonal = 0, subDiagonal = 0;

            for (int i = 0; i < size; i++)
            {
                int[] arrayInput = Array.ConvertAll(Console.ReadLine().Split(" "), int.Parse);
                for (int j = 0; j < size; j++)
                {
                    if (i == j)
                    {
                        mainDiagonal += arrayInput[j];

                    }
                    if (i + j == size - 1)
                    {
                        subDiagonal += arrayInput[j];
                    }
                    matrix[i,j] = arrayInput[j];

                }
            }
            int diagonalDiff = Math.Abs(mainDiagonal - subDiagonal);
            Printing.Array2Dim(matrix);
            Console.WriteLine($"Diagonal difference = {diagonalDiff}");

        }
    }
}
