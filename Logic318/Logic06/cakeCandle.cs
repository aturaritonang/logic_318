﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic06
{
    internal class cakeCandle
    {
        public cakeCandle()
        {
            Console.WriteLine("--Birthday Cake Candle--");
            Console.Write("Masukkan deret tinggi lilin : ");
            string[] angka = Console.ReadLine().Split(' ');
            int[] arr = Array.ConvertAll<string, int>(angka, int.Parse);

            int max = arr[0];
            int hasilmax = 0;

            for (int i = 1; i < arr.Length; i++)
            {
                if (arr[i] > max)
                {
                    max = arr[i];
                    hasilmax = 0;
                }
                if (arr[i] == max)
                {
                    hasilmax++;
                }
            }
            Console.WriteLine($"lilin tertinggi adalah {max} dengan jumlah {hasilmax}");
    }
    }
}
