﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic06
{
    internal class dimensionCaseStudy
    {
        public dimensionCaseStudy()
        {
            Console.WriteLine("===Dimension Case Study===");
            Console.WriteLine("===Min - Max Sum       ===");
            Console.Write("Masukkan deret angka (pemisah pakai spasi) : ");
            string[] angka = Console.ReadLine().Split(' ');
            int[] arr = Array.ConvertAll<string, int>(angka, int.Parse);

            int sum = 0;
            int min = arr.Min();
            int max = arr.Max();
            for (int i = 0; i < arr.Length; i++)
            {
                sum = sum + arr[i];
            }
            Console.WriteLine();
            Console.WriteLine($" Penjumlahan terkecil = {sum - max}");
            Console.WriteLine($" Penjumlahan terbesar = {sum - min}");
        }
    }
}
