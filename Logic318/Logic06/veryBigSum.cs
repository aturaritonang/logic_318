﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic06
{
    internal class veryBigSum
    {
        public veryBigSum()
        {
            Console.Write("input nilai : ");
            int T = Convert.ToInt16(Console.ReadLine());
            Int64 total = 0;
            foreach (string str in Console.ReadLine().Split(' '))
            {
                total += Convert.ToInt64(str);
            }
            Console.WriteLine(total);
        }
    }
}
