﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic07
{
    internal class CaesarChipper
    {
        public CaesarChipper()
        {
            Console.WriteLine("--Caesar Chipper");
            Console.Write("Masukkan password : ");
            string password = Console.ReadLine();
            Console.Write("Masukkan angka rotasi : ");
            int rotasi = int.Parse(Console.ReadLine());

            //kode low level
            if (rotasi > 26)
                rotasi = rotasi % 26;

            for (int i = 0; i < password.Length; i++)
            {
                int asciiCode = (int)password[i];
                if (asciiCode <= 122 && asciiCode > 96)
                {
                    //huruf kecil
                    if (asciiCode + rotasi <= 122)
                        Console.Write((char)(((int)password[i]) + rotasi));
                    else
                    {
                        int offset = rotasi - (122 - asciiCode);
                        Console.Write((char)(96 + offset));
                    }
                }
                else if (asciiCode <= 90 && asciiCode > 64)
                {
                    //huruf besar
                    if (asciiCode + rotasi <= 90)
                        Console.Write((char)(((int)password[i]) + rotasi));
                    else
                    {
                        int offset = rotasi - (90 - asciiCode);
                        Console.Write((char)(64 + offset));
                    }
                }
                else
                    Console.Write(password[i]);
            }
        }
    }
}
