﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic07
{
    internal class Pangram
    {
        public Pangram()
        {
            Console.Write("Masukkan kalimat : ");
            string kalimat = Console.ReadLine().ToLower();

            string alfabet = "abcdefghijklmnopqrstuvwxyz";
            bool isPangram = true;
            foreach (char item in alfabet)
            {
                if (!kalimat.Contains(item))
                {
                    isPangram = false;
                    
                }
            }
            if (isPangram)
            {
                Console.WriteLine("Kalimat ini Pangram");
            }
            else
            {
                Console.WriteLine("Kalimat ini bukan Pangram");
            }

        }
    }
}
