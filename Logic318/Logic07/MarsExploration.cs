﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic07
{
    internal class MarsExploration
    {
        public MarsExploration()
        {
            Console.WriteLine("---SOS---");
            Console.Write("\nMasukkan Sinyal SOS : ");
            char[] sinyal = Console.ReadLine().ToUpper().ToCharArray();

            int pembanding = 0;
            int countB = 0;
            string tmp = "";

            if (sinyal.Length % 3 != 0)
            {
                Console.WriteLine("Invalid!");
            }
            else
            {
                for (int i = 0; i < sinyal.Length; i += 3)
                {
                    if (sinyal[i] != 'S' || sinyal[i + 1] != 'O' || sinyal[i + 2] != 'S')
                    {
                        pembanding++;
                        //tmp += "SOS";
                    }
                    else
                    {
                        tmp += sinyal[i].ToString() + sinyal[i + 1].ToString() + sinyal[i + 2].ToString();
                        countB++;
                    }
                }
                Console.WriteLine($"Total sinyal salah : {pembanding}");
                Console.WriteLine($"Total sinyal benar : {countB}");
                Console.WriteLine($"Sinyal yang diterima : {String.Join("", sinyal)}");
                Console.WriteLine($"Sinyal yang benar : {tmp}");
            }

        }
    }
}
