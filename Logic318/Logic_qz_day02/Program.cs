﻿
//lingkaran();
//modulus();
//puntungRokok();
//gradeNilai();
//ganjilGenap();
menu();
Console.ReadKey();

static void menu()
{
    bool ulangi = true;
    int input;
    while (ulangi)
    {
        Console.WriteLine("\n----Menu tugas Logic 03----");
        Console.WriteLine("|   1. Lingkaran           |");
        Console.WriteLine("|   2. Persegi             |");
        Console.WriteLine("|   3. Modulus             |");
        Console.WriteLine("|   4. Puntung Rokok       |");
        Console.WriteLine("|   5. Grade Nilai         |");
        Console.WriteLine("|   6. Ganjil Genap        |");

        Console.Write("Pilih menu : ");
        input = int.Parse(Console.ReadLine());

        switch (input)
        {
            case 1:
                lingkaran();
                break;
            case 2:
                persegi();
                break;
            case 3:
                modulus();
                break;
            case 4:
                puntungRokok();
                break;
            case 5:
                gradeNilai();
                break;
            case 6:
                ganjilGenap();
                break;
            default:
                Console.WriteLine("Masukkan hanya 1 - 6 dan angka!");
                break;
        }
        ulangi = false;
    }
}

static void kembali()
{
    Console.WriteLine("\nApakah ingin kembali ke menu ? (y/n) ");
    string menuInput = Console.ReadLine().ToLower();
    if (menuInput == "y")
    {
        Console.Clear();
        menu();
    }
    else
    {
        Console.WriteLine("Terima kasih!");
        Console.WriteLine("Silahkan tekan Enter kembali...");
    }
}


static void lingkaran()
{
    double kel, luas;
    double phi = 22 / 7;
    Console.WriteLine("--1.Keliling dan Luas Lingkaran--");
    Console.Write("Masukkan jari-jari (r) : ");
    double jari = double.Parse(Console.ReadLine());

    kel = (2 * jari) * 22 / 7 ;
    luas = (jari * jari) * 22 / 7;

    Console.WriteLine("Hasil rumus keliling lingkaran = " + kel + "cm\xb2");
    Console.WriteLine("Hasil rumus luas lingkaran = " + luas + "cm\xb2");

    kembali();
}

static void persegi()
{
    Console.WriteLine("--2.Keliling dan Luas Persegi--");
    int kelilingpersegi, luaspersegi, s;
    Console.Write("Masukkan Sisi Persegi: ");
    s = int.Parse(Console.ReadLine());

    kelilingpersegi = 4 * s;
    luaspersegi = s * s;

    Console.WriteLine("Hasil Keliling Persegi " + kelilingpersegi);
    Console.WriteLine("Hasil Luas Persegi " + luaspersegi);

    kembali();
}

static void modulus()
{
    Console.WriteLine("--3.Modulus--");
    Console.Write("Masukkan angka : ");
    int angka = int.Parse(Console.ReadLine());
    Console.Write("Masukkan angka pembagi : ");
    int pembagi = int.Parse(Console.ReadLine());

    int hasil = angka % pembagi;

    if (hasil == 0)
    {
        Console.WriteLine($"Angka {angka} % {pembagi} adalah {hasil}");
    }
    else
    {
        Console.WriteLine($"Angka {angka} % {pembagi} bukan 0 melainkan {hasil}");
    }

    kembali();
}

static void puntungRokok()
{
    int puntung, batang, sisa, harga;
    Console.WriteLine("--4.Puntung Rokok--");
    Console.Write("Masukkan puntung rokok : ");
    puntung = int.Parse(Console.ReadLine());

    batang = puntung / 8;
    harga = batang * 500;
    sisa = puntung % 8;

    if (puntung % 8 == 0 || puntung > 7)
    {
        int kurang = 8 - sisa;
        Console.WriteLine($"8 puntung menjadi 1 batang rokok, hasil {batang} batang x 500 = Rp.{harga}");
        Console.WriteLine($"Sisa {sisa} puntung");
        //Console.WriteLine($"Kurang {kurang} puntung");
    }
    else
    {
        Console.WriteLine($"Puntung masih kurang menjadi 1 batang baru ada {puntung} puntung");
    }

    kembali();
}

static void gradeNilai()
{
    Console.WriteLine("--5.Grade Nilai--");
    Console.Write("Masukkan nilai = ");
    int nilai = int.Parse(Console.ReadLine());

    if (nilai >= 80 && nilai <= 100)
    {
        Console.WriteLine("Nilai anda mendapatkan grade A");
    }
    else if (nilai >= 60 && nilai < 80)
    {
        Console.WriteLine("Nilai anda mendapatkan grade B");
    }
    else if (nilai >= 0 && nilai < 60)
    {
        Console.WriteLine("Nilai anda mendapatkan grade C");
    }
    else
    {
        Console.WriteLine("Nilai yang anda inputkan tidak sesuai!");
    }
    kembali();
}

static void ganjilGenap()
{
    Console.WriteLine("--6.Cek Angka Ganjil / Genap--");
    Console.Write("Masukkan angka : ");
    int angka = int.Parse(Console.ReadLine());

    if (angka % 2 == 0)
    {
        Console.WriteLine($"angka {angka} adalah genap");
    }
    else
    {
        Console.WriteLine($"angka {angka} adalah ganjil");
    }
    kembali();
}