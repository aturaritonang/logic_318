﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utility;

namespace Logic05
{
	public class Soal03
	{
		public Soal03()
		{
			//1   4   7   10  13  16  19

			Console.Write("Masukkan N: ");
			int jumlah = int.Parse(Console.ReadLine());
			string[] arrString = new string[jumlah];
			//int angka = 1;
			for (int i = 0; i < jumlah; i++)
			{
				//Console.Write((i * 3 + 1) + "\t");
				//angka += 3;
				arrString[i] = (i * 3 + 1).ToString();
			}
			Printing.Array1Dim(arrString);
		}

	}
}
